import './style.scss'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

const app = createApp( App, {
    mixins: {
        isLogin() {
            return sessionStorage.getItem( 'token' )
        }
    }
} )

app.use(router)
app.mount('#app')
