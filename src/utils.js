import router from "./router"

export function redirectToLogin() {
    router.push( { name: 'login' } )
}

export function checkJWT( error ) {
    if ( error?.response?.data?.error == 'JWT expired' ) {
        redirectToLogin()
    }
}

export function getJWTData() {
    const token = sessionStorage.getItem( 'token' )
    
    if ( token ) {
        return JSON.parse( atob( token.split('.')[1] ) )
    }

    return null
}