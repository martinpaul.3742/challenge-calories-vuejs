import { createRouter, createWebHistory } from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'
import RecipeRead from './components/RecipeRead.vue'
import RecipeCreate from './components/RecipeCreate.vue'
import RecipeUpdate from './components/RecipeUpdate.vue'

// const router = createRouter({
//   history: createWebHistory(import.meta.env.BASE_URL),
//   routes: [
//     {
//       path: '/',
//       name: 'home',
//       component: HomeView
//     },
//     {
//       path: '/movies',
//       name: 'moviesList',
//       component: MoviesList
//     },
//     {
//       path: '/movies/:filmId',
//       name: 'moviesDetails',
//       component: MoviesDetails
//     }
   
//   ]
// })


const router = createRouter( {
	history: createWebHistory( import.meta.env.BASE_URL ),
	routes: [
		{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/recipe-:recipeId',
			name: 'recipe',
			component: RecipeRead
		},
		{
			path: '/recipe-create',
			name: 'recipe-create',
			component: RecipeCreate
		},
		{
			path: '/recipe-update-:recipeId',
			name: 'recipe-update',
			component: RecipeUpdate
		},
	]
} )

export default router
